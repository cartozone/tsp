function maybeLoadGeojson(script_id, callback) {
  const script = document.getElementById(script_id);

  if (script !== null) {
    const data = JSON.parse(script.textContent);

    callback(data);
  }
}

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('file').addEventListener('change', () => {
    document.getElementById('form').submit();
  });

  const view = new ol.View({
    center: [-99.13805007934569, 19.416411431775522],
    zoom: 13,
    projection: 'EPSG:4326',
  });
  const map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM(),
      }),
    ],
    view,
  });

  maybeLoadGeojson('points', (data) => {
    const vectorSource = new ol.source.Vector({
      features: new ol.format.GeoJSON().readFeatures(data),
    });

    const vectorLayer = new ol.layer.Vector({
      source: vectorSource,
      style: new ol.style.Style({
        image: new ol.style.Icon({
          src: '/static/img/dot.png',
          anchor: [.5, .5],
        }),
      }),
    });

    map.addLayer(vectorLayer);

    view.fit(vectorSource.getExtent());
  });

  maybeLoadGeojson('result_line', (data) => {
    const vectorSource = new ol.source.Vector({
      features: new ol.format.GeoJSON().readFeatures(data),
    });

    const vectorLayer = new ol.layer.Vector({
      source: vectorSource,
      style: () => {
        const styles = [
          // linestring
          new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: '#3EA46D',
              width: 4,
            }),
          }),
        ];

        return styles;
      },
    });

    map.addLayer(vectorLayer);

    view.fit(vectorSource.getExtent());

    // now create a link to download the data
    const blob = new Blob([
      (new ol.format.KML())
      .writeFeatures(vectorSource.getFeatures(), {
        featureProjection: 'EPSG:4326',
      })
    ], {type : 'application/vnd.google-earth.kml+xml'});
    const objectURL = URL.createObjectURL(blob);

    document.getElementById('download-link').href = objectURL;

    document.getElementById('downloads').classList.add('show');
  });
});
