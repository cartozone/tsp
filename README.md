# Traveling salesman problem

Un sencillísimo proyecto que resuleve este problema usando las características
de pgrouting

## Desarrollo

Para levantar el servidor de desarrollo es necesario tener variables de entorno:

    export FLASK_APP=tsp
    export FLASK_DEBUG=1

que pueden estar en un archivo `.env`. Y luego hacer:

    pipenv run flask run
