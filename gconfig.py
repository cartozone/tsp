APP_NAME = 'tsp'

pidfile = '/run/tsp/{}.pid'.format(APP_NAME)
bind = ['unix:/run/tsp/{}.sock'.format(APP_NAME)]
user = 'tsp'
group = 'tsp'
accesslog = '/var/log/tsp/{}.access.log'.format(APP_NAME)
errorlog = '/var/log/tsp/{}.error.log'.format(APP_NAME)
raw_env = [
]
capture_output = True
worker_class = 'eventlet'
workers = 3
