-- en que orden visitar los nodos (tsp)
SELECT * FROM pgr_TSP(
  $$
  SELECT * FROM pgr_dijkstraCostMatrix(
    'SELECT gid as id, source, target, cost, reverse_cost FROM ways',
    (SELECT array_agg(id) FROM ways_vertices_pgr WHERE id in (70, 26, 160)),
    directed => false)
  $$, 70);

-- como lo de arriba, pero devolviendo las coordenadas de los nodos visitados
SELECT wvp.lon, wvp.lat FROM pgr_TSP(
  $$
  SELECT * FROM pgr_dijkstraCostMatrix(
    'SELECT gid as id, source, target, cost, reverse_cost FROM ways',
    (SELECT array_agg(id) FROM ways_vertices_pgr WHERE id in (90724,125010,126030,118800,119340,8760,124789,8760,8695,8695,126682,116513,8696,119401,119401,8761,118327,117213,117213,119401,137158,8760,118327,116513,113270,116513,136981,119401,8697,8697,117213,119340,119340,394,113270,113270,113269,89601,166885,118448,43088,166885,118447,118448,89601,136980,154672,154672,118800,113379,8699,166853,8773,8781,166853,118446,117184,117184,117213,117213,117214,124847,117214,66666,118447,8781,166887,46219,113379,113379,402,398,8699,166886,119169,113357,46216,46218,113357,166883,41136,41136,166884,46218,398,43088,46219,8697,402,46219,8696,8696,402,8690,8690,402,402,114480,114480,50255,113271,128036,166714,50255,50255,50255,174819,398,398,8699,398,173509,8699,46216,119169,166978,119169,117214,8694,8695,114480,128036,114480,117184,8781,10479)),
    directed => false)
  $$, 10479)
  LEFT JOIN ways_vertices_pgr wvp ON wvp.id = node;

-- encontrar la ruta más corta entre dos nodos
SELECT node, wvp.lon, wvp.lat FROM pgr_dijkstra(
    'SELECT gid as id, source, target, cost, reverse_cost FROM ways',
    70, 160
) left join ways_vertices_pgr wvp on node = wvp.id;

-- encontrar el nodo más cercano a cada punto de una colección
WITH a(id, geom) as (
    VALUES
    (1, ST_SetSRID(ST_Point(-99.14032459259033,19.43591903740902), 4326))
)
SELECT
    a.id as orig, b.id as closest
FROM a
CROSS JOIN LATERAL (
    SELECT wvp.id
    FROM ways_vertices_pgr wvp
    ORDER BY wvp.the_geom <-> a.geom
    LIMIT  1
) AS b;
