-- Following query used to create a table named nodos
SELECT node
FROM pgr_TSP(
    $$
    SELECT *
    FROM pgr_dijkstraCostMatrix(
        'SELECT gid as id, source, target, cost, reverse_cost FROM osm2pgr.ways',
        (
            SELECT array_agg(a.id)
            from osm2pgr.ruta_15_lunes, lateral (
                select id
                from osm2pgr.ways_vertices_pgr
                order by osm2pgr.ways_vertices_pgr.the_geom <-> osm2pgr.ruta_15_lunes.geom
                limit 1
            ) a
        ),
        directed => true
    )
    $$,
    start_id => 2506
);

-- this query yields the actual route from the ordered list of nodes returned
-- by the previous query
SELECT ruta.start_vid, ruta.end_vid, ruta.node, st_astext(b.the_geom)
FROM pgr_dijkstraVia(
    'SELECT gid as id, source, target, cost, reverse_cost FROM osm2pgr.ways order by id',
    ARRAY(SELECT nodos.node FROM osm2pgr.nodos),
    true
) ruta
INNER JOIN osm2pgr.ways b ON ruta.edge = b.gid;

-- A complete example with points from user
SELECT node
FROM pgr_TSP(
    $$
    SELECT *
    FROM pgr_dijkstraCostMatrix(
        'SELECT gid as id, source, target, cost, reverse_cost FROM osm2pgr.ways',
        (
            with original_point_set(geom) as (
                VALUES
                (ST_SetSRID(ST_Point(-99.163, 19.392), 4326)),
                (ST_SetSRID(ST_Point(-99.164, 19.395), 4326)),
                (ST_SetSRID(ST_Point(-99.165, 19.391), 4326)),
                (ST_SetSRID(ST_Point(-99.162, 19.394), 4326)),
                (ST_SetSRID(ST_Point(-99.163, 19.397), 4326))
            )
            SELECT array_agg(a.id)
            from original_point_set, lateral (
                select id
                from osm2pgr.ways_vertices_pgr
                order by osm2pgr.ways_vertices_pgr.the_geom <-> original_point_set.geom
                limit 1
            ) a
        ),
        directed => true
    )
    $$,
    start_id => 1040
);

-- this query yields the actual route from the ordered list of nodes returned
-- by the previous query
SELECT ruta.node, st_astext(b.the_geom)
FROM pgr_dijkstraVia(
    'SELECT gid as id, source, target, cost, reverse_cost FROM osm2pgr.ways order by id',
    ARRAY(
        SELECT node
        FROM pgr_TSP(
            $$
            SELECT *
            FROM pgr_dijkstraCostMatrix(
                'SELECT gid as id, source, target, cost, reverse_cost FROM osm2pgr.ways',
                (
                    WITH original_point_set(geom) AS (
                        VALUES
                        (ST_SetSRID(ST_Point(-99.163, 19.392), 4326)),
                        (ST_SetSRID(ST_Point(-99.164, 19.395), 4326)),
                        (ST_SetSRID(ST_Point(-99.165, 19.391), 4326)),
                        (ST_SetSRID(ST_Point(-99.162, 19.394), 4326)),
                        (ST_SetSRID(ST_Point(-99.163, 19.397), 4326))
                    )
                    SELECT array_agg(a.id)
                    from original_point_set, lateral (
                        select id
                        from osm2pgr.ways_vertices_pgr
                        order by osm2pgr.ways_vertices_pgr.the_geom <-> original_point_set.geom
                        limit 1
                    ) a
                ),
                directed => true
            )
            $$,
            start_id => 1040
        )
    ),
    true
) ruta
INNER JOIN osm2pgr.ways_vertices_pgr b ON ruta.node = b.id;

-- compute a cost matrix using the points in the grapth that are closest to the
-- given points. This will give you the closest point in the graph to every
-- point you give as input, which is much better than using the closest vertex
--
-- ids for source points are hardcoded as start_vids for the third parameter of
-- the pgr_withPointsCostMatrix function
SELECT * FROM pgr_withPointsCostMatrix(
    'SELECT gid as id, source, target, cost, reverse_cost FROM ways',
    'WITH original_points(id, geom) AS (
        VALUES
        (1, ST_SetSRID(ST_Point(-99.163, 19.392), 4326)),
        (2, ST_SetSRID(ST_Point(-99.164, 19.395), 4326)),
        (3, ST_SetSRID(ST_Point(-99.165, 19.391), 4326)),
        (4, ST_SetSRID(ST_Point(-99.162, 19.394), 4326)),
        (5, ST_SetSRID(ST_Point(-99.163, 19.397), 4326))
    )
    SELECT
        original_points.id AS pid,
        edg.edge_id,
        edg.fraction
    FROM original_points
    CROSS JOIN LATERAL (
        SELECT
            gid AS edge_id,
            ST_LineLocatePoint(the_geom, original_points.geom) AS fraction
        FROM ways
        ORDER BY the_geom <-> original_points.geom
        LIMIT  1
    ) AS edg',
    ARRAY [-1, -2, -3, -4, -5],
    directed => true,
    driving_side => 'r'
);

-- A complete example of routing but using the pgr_withPointsCostMatrix for more
-- accurate results
SELECT ruta.node, st_astext(b.the_geom)
FROM pgr_dijkstraVia(
    'SELECT gid as id, source, target, cost, reverse_cost FROM ways order by id',
    ARRAY(
        SELECT node
        FROM pgr_TSP(
            $$
            SELECT * FROM pgr_withPointsCostMatrix(
                'SELECT gid as id, source, target, cost, reverse_cost FROM ways',
                'WITH original_points(id, geom) AS (
                    VALUES
                    (1, ST_SetSRID(ST_Point(-99.163, 19.392), 4326)),
                    (2, ST_SetSRID(ST_Point(-99.164, 19.395), 4326)),
                    (3, ST_SetSRID(ST_Point(-99.165, 19.391), 4326)),
                    (4, ST_SetSRID(ST_Point(-99.162, 19.394), 4326)),
                    (5, ST_SetSRID(ST_Point(-99.163, 19.397), 4326))
                )
                SELECT
                    original_points.id AS pid,
                    edg.edge_id,
                    edg.fraction
                FROM original_points
                CROSS JOIN LATERAL (
                    SELECT
                        gid AS edge_id,
                        ST_LineLocatePoint(the_geom, original_points.geom) AS fraction
                    FROM ways
                    ORDER BY the_geom <-> original_points.geom
                    LIMIT  1
                ) AS edg',
                ARRAY [-1, -2, -3, -4, -5],
                directed => false,
                driving_side => 'r'
            )
            $$,
            start_id => -1
        )
    ),
    true
) ruta
INNER JOIN ways_vertices_pgr b ON ruta.node = b.id;
