from functools import partial
from typing import List, Tuple
import csv
import sys

from flask import Flask, render_template, request, flash, redirect
from flask_sqlalchemy import SQLAlchemy
import toml

app = Flask(__name__)

app.config.from_file('config.toml', load=toml.load)

db = SQLAlchemy(app)

error = partial(flash, category='error')


def closest_from_grid(points: List[Tuple[float, float]]):
    ''' returns the ids of the intersections of the grid in the database that
    are closest to each of the given points '''
    origs = ','.join([
        f'({i+1}, ST_SetSRID(ST_Point({lon},{lat}), 4326))'
        for i, (lon, lat) in enumerate(points)
    ])

    closest = db.engine.execute(f'''
with a(id, geom) as (
    values
    {origs}
)
select
    b.id as closest
FROM a
CROSS JOIN LATERAL (
  SELECT wvp.id
  FROM ways_vertices_pgr wvp
  ORDER BY
    wvp.the_geom <-> a.geom
  LIMIT  1
) AS b;''').all()

    return closest


def to_geojson(data: List[Tuple[float, float]]):
    return {
      "type": "FeatureCollection",
      "features": [
        {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [lon, lat],
          }
        }
        for (lon, lat) in data
      ]
    }


def solve_tsp(points: List[Tuple[float, float]], start: Tuple[float, float]) -> List[Tuple[float, float]]:
    '''given a list of coordinates and a start coordinate solves the tsp
    problem. The return value is a refined list of points that can be followed
    to accomplish the goal but do not correspond to the original ones. Instead
    they belong to the street grid.'''
    start_node = closest_from_grid([start])[0][0]
    points = ','.join(
        f'(ST_SetSRID(ST_Point({p[0]}, {p[1]}), 4326))'
        for p in points
    )

    query = f'''
SELECT st_astext(street_nodes.the_geom)
FROM pgr_dijkstraVia(
    'SELECT gid as id, source, target, cost, reverse_cost FROM ways order by id',
    ARRAY(
        SELECT node
        FROM pgr_TSP(
            $$
            SELECT *
            FROM pgr_dijkstraCostMatrix(
                'SELECT gid as id, source, target, cost, reverse_cost FROM ways',
                (
                    with original_point_set(geom) as (
                        VALUES
                        {points}
                    )
                    SELECT array_agg(closest_nodes.id)
                    from original_point_set, lateral (
                        select id
                        from ways_vertices_pgr
                        order by ways_vertices_pgr.the_geom <-> original_point_set.geom
                        limit 1
                    ) closest_nodes
                ),
                directed => false
            )
            $$,
            start_id => {start_node}
        )
    ),
    true
) ruta
INNER JOIN ways_vertices_pgr street_nodes ON ruta.node = street_nodes.id;
'''
    points = db.engine.execute(query).all()

    return points


@app.route("/")
def hello_world():
    return render_template("index.html")


@app.route("/", methods=["POST"])
def process():
    if 'file' not in request.files:
        error('Por favor selecciona un archivo')
        return redirect(request.url)

    file = request.files['file']
    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    if file.filename == '':
        error('Por favor selecciona un archivo')
        return redirect(request.url)

    contents = file.read()

    try:
        decoded = contents.decode('utf8')
    except UnicodeDecodeError:
        error('Parece que tu archivo no es UTF-8 válido. Prueba exportándolo de nuevo')
        return redirect(request.url)

    reader = csv.DictReader(decoded.split('\n'))
    data = []
    start_node = None

    try:
        for row in reader:
            data.append((row['lon'], row['lat']))
            if row['is_start']:
                start_node = (row['lon'], row['lat'])
    except KeyError as e:
        error(f'Falta la columna {e} en el archivo CSV')
        return redirect(request.url)

    if start_node is None:
        error('En el archivo de entrada no hay ningún registro marcado como inicio')
        return redirect(request.url)

    points = solve_tsp(data, start_node)

    coords = (p[0][6:-1].split() for p in points)

    nodes = [[float(lon), float(lat)] for lon, lat in coords]

    line = {
      "type": "FeatureCollection",
      "features": [
        {
          "type": "Feature",
          "properties": {},
          "geometry": {
            "type": "LineString",
            "coordinates": nodes,
          }
        }
      ]
    }

    return render_template("index.html", **{
        'points': to_geojson(data),
        'result_line': line,
    })
